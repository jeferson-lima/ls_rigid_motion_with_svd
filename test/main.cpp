/* ----------------------------------------------------------------------------
 * Copyright 2021, Jeferson Lima
 * All Rights Reserved
 * See LICENSE for the license information
 * -------------------------------------------------------------------------- */

/**
 *  @file   two_pclouds.cpp
 *  @author Jeferson Lima
 *  @brief  Test file for Least Squares Point CLoud for Homogeneous Transformation
 *  @date   May 14, 2021
 **/

#include "ls_transformation.hpp"
#include <string>
#include <yaml-cpp/yaml.h>

#define CONFIGS_YAML "../config/configs.yaml"
#define OUTPUT_FILE  "../data/output.ply"

using namespace std;

int main(int argc, char** argv)
{
  YAML::Node config = YAML::LoadFile(CONFIGS_YAML);

  //load configs
  const std::string curr_sample = config["curr_sample"].as<std::string>();
  const std::string prev_sample = config["prev_sample"].as<std::string>();
  const int iter  = config["iterations"].as<int>();
  const bool show_plot = config["show_plot"].as<bool>();

  pcl::PointCloud<pcl::PointXYZ>::Ptr curr_cloud (new pcl::PointCloud<pcl::PointXYZ>());
  pcl::PointCloud<pcl::PointXYZ>::Ptr prev_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
  pcl::PointCloud<pcl::PointXYZ>::Ptr copy_cloud (new pcl::PointCloud<pcl::PointXYZ> ());

  //load file
  if (pcl::io::loadPLYFile(curr_sample, *curr_cloud) < 0)
  {
    PCL_ERROR ("Error loading cloud %s.\n", curr_sample);
    return (-1);
  }
  *copy_cloud = *curr_cloud;

  if (prev_sample.compare("None") != 0)
  {
    //load file
    if (pcl::io::loadPLYFile(prev_sample, *prev_cloud) < 0)
    {
      PCL_ERROR ("Error loading cloud %s.\n", prev_sample);
      return (-1);
    }
    LeastSquareT LS(curr_cloud, prev_cloud, iter);
  }
  else
      {
        LeastSquareT LS(curr_cloud, iter);

        prev_cloud = LS._transformed_cloud;
        pcl::io::savePLYFileASCII(OUTPUT_FILE, *prev_cloud);

        config["prev_sample"] = OUTPUT_FILE;
        std::ofstream fcfg(CONFIGS_YAML); 
        fcfg << config; // update config.yaml
      }
  
  if (show_plot)
  {
    pcl::visualization::PCLVisualizer viewer ("Show Transformation");

    int v1 (0);
    int v2 (1);

    viewer.createViewPort (0.0, 0.0, 0.5, 1.0, v1);
    viewer.createViewPort (0.5, 0.0, 1.0, 1.0, v2); 
    // The color we will be using
    float bckgr_gray_level = 0.0;  // Black
    float txt_gray_lvl = 1.0 - bckgr_gray_level;

    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cloud_in_color_h (copy_cloud, 10, 150, 220);
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cloud_out_color_h (prev_cloud, 20, 180, 20);

    viewer.addPointCloud (copy_cloud, cloud_in_color_h, "cp_curr_cloud", v1);
    viewer.addPointCloud (prev_cloud, cloud_out_color_h, "prev_cloud", v1);

    viewer.addPointCloud (curr_cloud, cloud_in_color_h,  "curr_cloud", v2);
    viewer.addPointCloud (prev_cloud, cloud_out_color_h, "final_prev_cloud",  v2);

    viewer.addText ("Blue:  Original point cloud and\nGreen: Transformed point cloud with Noise", 
                    10, 15, 16, txt_gray_lvl, txt_gray_lvl, txt_gray_lvl, "curr_cloud_lb", v1);

    viewer.addText ("Blue:  Final point cloud and\nGreen: Transformed point cloud with Noise", 
                    10, 15, 16, txt_gray_lvl, txt_gray_lvl, txt_gray_lvl, "prev_cloud_lb", v2);

    // Set background color
    viewer.setBackgroundColor (bckgr_gray_level, bckgr_gray_level, bckgr_gray_level, v1);
    viewer.setBackgroundColor (bckgr_gray_level, bckgr_gray_level, bckgr_gray_level, v2);

    // Set camera position and orientation
    viewer.setCameraPosition (-2341.03, 4290.93, 4921.71,
                              0.782023, -0.211742, 0.586179,
                              610.049, 757.207, -291.802);
    viewer.setSize (960, 540);  // Visualiser window size

     while (!viewer.wasStopped ())
    {
      viewer.spinOnce ();
    }
  }
  
  return 0;
}
